package com.mmedrek.revolut.resource;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import com.mmedrek.revolut.MoneyTransferApplication;
import com.mmedrek.revolut.config.MoneyTransferConfig;
import com.mmedrek.revolut.model.Account;
import com.mmedrek.revolut.model.CreateAccountRequest;
import com.mmedrek.revolut.model.TransferRequest;

import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;

public class MoneyTransferResourceTest {

	@ClassRule
	public static final DropwizardAppRule<MoneyTransferConfig> RULE = new DropwizardAppRule<MoneyTransferConfig>(
			MoneyTransferApplication.class, ResourceHelpers.resourceFilePath("config.yaml"));

	private static Client CLIENT;

	@BeforeClass
	public static void setup() {
		CLIENT = new JerseyClientBuilder(RULE.getEnvironment()).build("test client");
	}

	@Test
	public void post_success() {

		Account sourceAccount = sendCreateAccountRequest(new CreateAccountRequest("USD", new BigDecimal("2000.02")));
		Account targetAccount = sendCreateAccountRequest(new CreateAccountRequest("USD", new BigDecimal(1000)));

		Currency currency = Currency.getInstance("USD");
		BigDecimal amount = new BigDecimal(200.00);
		TransferRequest request = new TransferRequest(sourceAccount.getId(), targetAccount.getId(), currency, amount);

		Response response = sendTransferRequest(request);

		targetAccount = sendGetAccount(targetAccount.getId());
		sourceAccount = sendGetAccount(sourceAccount.getId());

		assertEquals(200, response.getStatus());
		assertEquals(new BigDecimal("1800.02"), sourceAccount.getBalanceAmount());
		assertEquals(new BigDecimal("1200.00"), targetAccount.getBalanceAmount());
	}

	@Test
	public void post_validationFailure() {

		Account sourceAccount = sendCreateAccountRequest(new CreateAccountRequest("USD", new BigDecimal(2000)));
		Account targetAccount = sendCreateAccountRequest(new CreateAccountRequest("USD", new BigDecimal(1000)));

		Currency currency = Currency.getInstance("PLN");
		BigDecimal amount = new BigDecimal(200.00);
		TransferRequest request = new TransferRequest(sourceAccount.getId(), targetAccount.getId(), currency, amount);

		Response response = sendTransferRequest(request);

		assertEquals(422, response.getStatus());
	}

	private Response sendTransferRequest(TransferRequest request) {
		return CLIENT.target(String.format("http://localhost:%d/money-transfer", RULE.getLocalPort())).request()
				.post(Entity.json(request));
	}

	private Account sendCreateAccountRequest(CreateAccountRequest request) {
		return CLIENT.target(String.format("http://localhost:%d/account", RULE.getLocalPort())).request()
				.post(Entity.json(request), Account.class);
	}

	private Account sendGetAccount(UUID accountId) {
		return CLIENT.target(String.format("http://localhost:%d/account/%s", RULE.getLocalPort(), accountId)).request()
				.get(Account.class);
	}
}
