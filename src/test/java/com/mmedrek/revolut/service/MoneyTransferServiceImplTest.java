package com.mmedrek.revolut.service;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

import org.hibernate.Transaction;
import org.hibernate.context.internal.ManagedSessionContext;
import org.joda.money.CurrencyUnit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import com.mmedrek.revolut.config.MoneyTransferConfig;
import com.mmedrek.revolut.config.MoneyTransferConfig.LockParams;
import com.mmedrek.revolut.db.AccountDao;
import com.mmedrek.revolut.exception.ValidationException;
import com.mmedrek.revolut.model.Account;
import com.mmedrek.revolut.model.TransferRequest;
import com.mmedrek.revolut.service.MoneyTransferServiceImpl.BalanceException;
import com.mmedrek.revolut.service.MoneyTransferServiceImpl.TheSameAccountException;

import io.dropwizard.testing.junit.DAOTestRule;

public class MoneyTransferServiceImplTest {

	private MoneyTransferService service;
	
	private AccountDao accountDao;
	
	private MoneyTransferConfig configuration;

	@Rule
    public DAOTestRule database = DAOTestRule.newBuilder().addEntityClass(Account.class).build();
	
    @Before
    public void setUp() {
    	accountDao = new AccountDao(database.getSessionFactory());
    	configuration = Mockito.mock(MoneyTransferConfig.class);
    	LockParams lockParams = Mockito.mock(LockParams.class);
    	Mockito.when(lockParams.getWaitTime()).thenReturn(100l);
    	Mockito.when(configuration.getLockParams()).thenReturn(lockParams);
    	service = new MoneyTransferServiceImpl(accountDao, configuration);
    }

	@Test
	public void transfer_success() throws Exception {
		//given
		Transaction transaction = database.getSessionFactory().getCurrentSession().beginTransaction();
		Account sourceAccount = accountDao.create(new Account("USD", new BigDecimal(1000)));
		Account targetAccount = accountDao.create(new Account("USD", new BigDecimal(2000)));
		transaction.commit();
		
		Currency currency = Currency.getInstance("USD");	
		BigDecimal amount = new BigDecimal(200);
		TransferRequest request = new TransferRequest(sourceAccount.getId(), targetAccount.getId(), currency , amount);
		
		//when
		doInDbTransaction(() -> transfer(request));
		
		//then
		Account savedSourceAccount = accountDao.getById(sourceAccount.getId()).orElseThrow(() -> new Exception("Source account not found"));
		Account savedTargetAccount = accountDao.getById(targetAccount.getId()).orElseThrow(() -> new Exception("targetAccount account not found"));
		
		Assert.assertEquals(new BigDecimal("800.00"), savedSourceAccount.getBalanceAmount());
		Assert.assertEquals(new BigDecimal("2200.00"), savedTargetAccount.getBalanceAmount());
		Assert.assertEquals(CurrencyUnit.USD, CurrencyUnit.of(savedSourceAccount.getBalanceCurrencyCode()));
		Assert.assertEquals(CurrencyUnit.USD, CurrencyUnit.of(savedTargetAccount.getBalanceCurrencyCode()));
	}
	
	@Test(expected = BalanceException.class)
	public void transfer_validation_unsufficientFunds() throws Exception {
		//given
		Transaction transaction = database.getSessionFactory().getCurrentSession().beginTransaction();
		Account sourceAccount = accountDao.create(new Account("USD", new BigDecimal(1000)));
		Account targetAccount = accountDao.create(new Account("USD", new BigDecimal(2000)));
		transaction.commit();
		
		Currency currency = Currency.getInstance("USD");	
		BigDecimal amount = new BigDecimal(2000000);
		TransferRequest request = new TransferRequest(sourceAccount.getId(), targetAccount.getId(), currency , amount);
		
		//when
		service.transfer(request);
	}
	
	@Test(expected = BalanceException.class)
	public void transfer_validation_currency() throws Exception {
		//given
		Transaction transaction = database.getSessionFactory().getCurrentSession().beginTransaction();
		Account sourceAccount = accountDao.create(new Account("USD", new BigDecimal(1000)));
		Account targetAccount = accountDao.create(new Account("USD", new BigDecimal(2000)));
		transaction.commit();
		
		Currency currency = Currency.getInstance("PLN");	
		BigDecimal amount = new BigDecimal(200);
		TransferRequest request = new TransferRequest(sourceAccount.getId(), targetAccount.getId(), currency , amount);
		
		//when
		service.transfer(request);
	}
	
	@Test(expected = TheSameAccountException.class)
	public void transfer_validation_theSameAccount() throws Exception {
		//given
		Transaction transaction = database.getSessionFactory().getCurrentSession().beginTransaction();
		Account account = accountDao.create(new Account("USD", new BigDecimal(1000)));
		transaction.commit();
		
		Currency currency = Currency.getInstance("PLN");	
		BigDecimal amount = new BigDecimal(200);
		TransferRequest request = new TransferRequest(account.getId(), account.getId(), currency , amount);
		
		//when
		service.transfer(request);
	}
	
	@Test
	public void transfer_multiple() throws Exception {
		//given
		Transaction transaction = database.getSessionFactory().getCurrentSession().beginTransaction();
		Account sourceAccount = accountDao.create(new Account("USD", new BigDecimal(1000)));
		Account targetAccount = accountDao.create(new Account("USD", new BigDecimal(2000)));
		transaction.commit();
		
		Currency currency = Currency.getInstance("USD");	
		BigDecimal amount = new BigDecimal(100);
		TransferRequest request = new TransferRequest(sourceAccount.getId(), targetAccount.getId(), currency , amount);
		
		int threadsCount = 5;
		
		//when
		CompletableFuture<Void> allOf = CompletableFuture.allOf(createTranserCompletableFutures(request, threadsCount));
		allOf.get();
		
		//then
		Account savedSourceAccount = accountDao.getByIdAndRefresh(sourceAccount.getId()).orElseThrow(() -> new Exception("Source account not found"));
		Account savedTargetAccount = accountDao.getByIdAndRefresh(targetAccount.getId()).orElseThrow(() -> new Exception("targetAccount account not found"));
		
		Assert.assertEquals(new BigDecimal("500.00"), savedSourceAccount.getBalanceAmount());
		Assert.assertEquals(new BigDecimal("2500.00"), savedTargetAccount.getBalanceAmount());
		Assert.assertEquals(CurrencyUnit.USD, CurrencyUnit.of(savedSourceAccount.getBalanceCurrencyCode()));
		Assert.assertEquals(CurrencyUnit.USD, CurrencyUnit.of(savedTargetAccount.getBalanceCurrencyCode()));
	}
	
	@SuppressWarnings("rawtypes")
	private CompletableFuture[] createTranserCompletableFutures(TransferRequest request, int size) {
		return IntStream.rangeClosed(1, size).mapToObj((it) -> CompletableFuture.runAsync(createTransferRunnable(request))).toArray(CompletableFuture[]::new);
	}
	
	private Runnable createTransferRunnable(TransferRequest request) {
		return () -> {
			try {
				ManagedSessionContext.bind(database.getSessionFactory().openSession());
				doInDbTransaction(() -> transfer(request));
				ManagedSessionContext.unbind(database.getSessionFactory());
			} catch (ValidationException e) {
				Assert.fail("Unable to create transfer runnable due to: " + e.getMessage());
			}
		};
	}
	
	private Void transfer(TransferRequest request) {
		service.transfer(request);
		return null;
	}
	
	@FunctionalInterface
	private interface TransactionTask<T> {
		T execute();
	}
	
	private <T> T doInDbTransaction(TransactionTask<T> supplier) {
		Transaction transaction = accountDao.beginTransaction();
		try {
			T result = supplier.execute();
			transaction.commit();
			return result;
		} catch (Exception e) {
			transaction.rollback();
			throw e;
		}
	}
}
