package com.mmedrek.revolut;

import org.flywaydb.core.Flyway;
import org.hibernate.SessionFactory;

import com.mmedrek.revolut.config.MoneyTransferConfig;
import com.mmedrek.revolut.db.AccountDao;
import com.mmedrek.revolut.exception.ValidationExceptionMapper;
import com.mmedrek.revolut.model.Account;
import com.mmedrek.revolut.resource.AccountResource;
import com.mmedrek.revolut.resource.MoneyTransferResource;

import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.db.PooledDataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class MoneyTransferApplication extends Application<MoneyTransferConfig> {
	
	private final HibernateBundle<MoneyTransferConfig> hibernate = new MoneyTransferHibernateBundle(
			Account.class);

	public static void main(String[] args) throws Exception {
		new MoneyTransferApplication().run(args);
	}

	@Override
	public void initialize(Bootstrap<MoneyTransferConfig> bootstrap) {
		super.initialize(bootstrap);
		bootstrap.addBundle(hibernate);
	}

	@Override
	public void run(MoneyTransferConfig configuration, Environment environment) throws Exception {
		migrateDatabase(configuration);
		
		AccountDao accountDao = new AccountDao(hibernate.getSessionFactory());
		environment.jersey().register(new MoneyTransferResource(accountDao, configuration));
		environment.jersey().register(new AccountResource(accountDao));
		environment.jersey().register(new ValidationExceptionMapper());
	}
	
	public SessionFactory getSessionFactory() {
		return hibernate.getSessionFactory();
	}
	
	private void migrateDatabase(MoneyTransferConfig configuration) {
		DataSourceFactory dataSourceFactory = configuration.getDataSourceFactory();
		Flyway flyway = Flyway.configure()
				.dataSource(dataSourceFactory.getUrl(), dataSourceFactory.getUser(), dataSourceFactory.getPassword())
				.load();
		flyway.migrate();
	}
	
	private static class MoneyTransferHibernateBundle extends HibernateBundle<MoneyTransferConfig>{

		public MoneyTransferHibernateBundle(Class<?> entity, Class<?>... entities) {
			super(entity, entities);
		}

		@Override
		public PooledDataSourceFactory getDataSourceFactory(MoneyTransferConfig configuration) {
			return configuration.getDataSourceFactory();
		}
	}
}
