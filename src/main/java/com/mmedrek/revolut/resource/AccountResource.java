package com.mmedrek.revolut.resource;

import java.net.URI;
import java.util.Currency;
import java.util.UUID;

import javax.validation.Valid;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.joda.money.CurrencyUnit;

import com.codahale.metrics.annotation.Timed;
import com.mmedrek.revolut.db.AccountDao;
import com.mmedrek.revolut.exception.ValidationException;
import com.mmedrek.revolut.model.Account;
import com.mmedrek.revolut.model.CreateAccountRequest;

import io.dropwizard.hibernate.UnitOfWork;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {

	private AccountDao accountDao;
	
	public AccountResource(AccountDao accountDao) {
		this.accountDao = accountDao;
	}
	
    @GET
    @Timed
    @UnitOfWork
    public Response getAll() throws ValidationException {
    	return Response.ok(accountDao.getAll()).build();
    }
    
    @Path("/{id}")
    @GET
    @Timed
    @UnitOfWork
    public Response get(@PathParam("id") UUID id) throws ValidationException {
    	return Response.ok(accountDao.getById(id)).build();
    }
    
    @POST
    @Timed
    @UnitOfWork
	public Response create(@Valid CreateAccountRequest accountRequest) {
        //TODO: move validate and creation to separate service
    	validateCurrency(accountRequest);
    	Account created = accountDao
				.create(new Account(accountRequest.getBalanceCurrencyCode(), accountRequest.getBalanceAmount()));
		return Response.created(URI.create("/account/" + created.getId())).entity(created).build();
	}
    
    private void validateCurrency(CreateAccountRequest accountRequest) {
    	try {
    		CurrencyUnit.of(Currency.getInstance(accountRequest.getBalanceCurrencyCode()));
    	} catch(Exception ex) {
    		throw new InvalidCurrencyException("Invalid currency", ex, accountRequest);
    	}
    }
    
    static class InvalidCurrencyException extends ValidationException {
		private static final long serialVersionUID = 1L;

		public InvalidCurrencyException(String message, Throwable th, CreateAccountRequest request) {
			super(message, th, request);
		}
    }
}
