package com.mmedrek.revolut.resource;

import javax.validation.Valid;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.mmedrek.revolut.config.MoneyTransferConfig;
import com.mmedrek.revolut.db.AccountDao;
import com.mmedrek.revolut.exception.ValidationException;
import com.mmedrek.revolut.model.TransferRequest;
import com.mmedrek.revolut.service.MoneyTransferService;
import com.mmedrek.revolut.service.MoneyTransferServiceImpl;
import com.mmedrek.revolut.utils.ObjectUtils;

import io.dropwizard.hibernate.UnitOfWork;

@Path("/money-transfer")
@Produces(MediaType.APPLICATION_JSON)
public class MoneyTransferResource {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MoneyTransferResource.class);

	private MoneyTransferService service;

	public MoneyTransferResource(AccountDao accountDao, MoneyTransferConfig configuration) {
		service = new MoneyTransferServiceImpl(accountDao, configuration);
	}

	@POST
	@Timed
	@UnitOfWork
	public Response transfer(@Valid TransferRequest request) throws ValidationException {
		LOGGER.info("Received money transfer request: {}", ObjectUtils.objectToString(request));
		service.transfer(request);
		LOGGER.info("Successfully processed money transfer request");
		return Response.ok(request).build();
	}
}
