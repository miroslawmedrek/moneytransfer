package com.mmedrek.revolut.exception;

public class ValidationException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	private Object request;

	public ValidationException(String message, Object request) {
		super(message);
		this.request = request;
	}
	
	public ValidationException(String message, Throwable th, Object request) {
		super(message, th);
		this.request = request;
	}
	
	public Object getRequest() {
		return request;
	}
}
