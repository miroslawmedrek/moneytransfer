package com.mmedrek.revolut.exception;

import java.text.MessageFormat;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mmedrek.revolut.utils.ObjectUtils;

/**Maps ValidationException to REST response*/
@Provider
public class ValidationExceptionMapper implements ExceptionMapper<ValidationException>{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ValidationExceptionMapper.class);
	
	public Response toResponse(ValidationException exception) {
		LOGGER.warn(MessageFormat.format("{0}. Request is: {1}", exception.getMessage(),
				ObjectUtils.objectToString(exception.getRequest())));
		return Response.status(422).entity(exception.getMessage()).type(MediaType.TEXT_PLAIN).build();
	}
}
