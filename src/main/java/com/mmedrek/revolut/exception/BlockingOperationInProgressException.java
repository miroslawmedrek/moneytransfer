package com.mmedrek.revolut.exception;

public class BlockingOperationInProgressException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public BlockingOperationInProgressException(String message) {
		super(message);
	}
}
