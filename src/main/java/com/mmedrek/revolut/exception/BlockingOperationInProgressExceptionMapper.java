package com.mmedrek.revolut.exception;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**Maps BlockingOperationInProgressException to REST response*/
@Provider
public class BlockingOperationInProgressExceptionMapper implements ExceptionMapper<BlockingOperationInProgressException>{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BlockingOperationInProgressExceptionMapper.class);
	
	public Response toResponse(BlockingOperationInProgressException exception) {
		LOGGER.warn("Deadlock detected. Reporting conflict", exception);
		return Response.status(409).entity(exception.getMessage()).type(MediaType.TEXT_PLAIN).build();
	}
}
