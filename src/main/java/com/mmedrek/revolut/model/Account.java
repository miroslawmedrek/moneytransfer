package com.mmedrek.revolut.model;

import java.math.BigDecimal;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.joda.money.Money;

@Entity
@Table(name = "ACCOUNT")
public class Account {

	@Id
	@Column(name = "ID", nullable = false)
	private UUID id;
	
	@Column(name = "BALANCE_CURRENCY_CODE", nullable = false)
	private String balanceCurrencyCode;
	
	@Column(name = "BALANCE_AMOUNT", nullable = false)
	private BigDecimal balanceAmount;
	
	protected Account() {	}
	
	public Account(String balanceCurrencyCode, BigDecimal balanceAmount) {
		id = UUID.randomUUID();
		this.balanceCurrencyCode = balanceCurrencyCode;
		this.balanceAmount = balanceAmount;
	}
	
	public UUID getId() {
		return id;
	}
	
	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}
	
	public String getBalanceCurrencyCode() {
		return balanceCurrencyCode;
	}
	
	public void setBalance(String balanceCurrencyCode, BigDecimal balanceAmount) {
		this.balanceAmount = balanceAmount;
		this.balanceCurrencyCode = balanceCurrencyCode;
	}
	
	public void setBalance(Money balance) {
		setBalance(balance.getCurrencyUnit().getCode(), balance.getAmount());
	}
}
