package com.mmedrek.revolut.model;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TransferRequest {

	private UUID requestId;
	
	@NotNull
	private UUID targetAccountId;
	
	@NotNull
	private UUID sourceAccountId;
	
	@NotNull
	private Currency currency;
	
	@NotNull
	private BigDecimal amount;
	
	@JsonCreator
	public TransferRequest(@JsonProperty("sourceAccountId") UUID sourceAccountId,
			@JsonProperty("targetAccountId") UUID targetAccountId,
			@JsonProperty("currency") Currency currency, @JsonProperty("amount") BigDecimal amount) {
		requestId = UUID.randomUUID();
		this.currency = currency;
		this.amount = amount;
		this.sourceAccountId = sourceAccountId;
		this.targetAccountId = targetAccountId;
	}
	
	public UUID getRequestId() {
		return requestId;
	}
	
	public UUID getSourceAccountId() {
		return sourceAccountId;
	}
	
	public UUID getTargetAccountId() {
		return targetAccountId;
	}
	
	public Currency getCurrency() {
		return currency;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
}
