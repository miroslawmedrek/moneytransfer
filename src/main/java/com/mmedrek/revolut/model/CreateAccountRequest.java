package com.mmedrek.revolut.model;

import java.math.BigDecimal;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CreateAccountRequest {

	@NotNull
	private String balanceCurrencyCode;
	@NotNull
	private BigDecimal balanceAmount;

	@JsonCreator
	public CreateAccountRequest(@JsonProperty("balanceCurrencyCode") String balanceCurrencyCode, 
			@JsonProperty("balanceAmount") BigDecimal balanceAmount) {
		this.balanceCurrencyCode = balanceCurrencyCode;
		this.balanceAmount = balanceAmount;
	}
	
	public BigDecimal getBalanceAmount() {
		return balanceAmount;
	}
	
	public String getBalanceCurrencyCode() {
		return balanceCurrencyCode;
	}
}
