package com.mmedrek.revolut.db;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.criteria.CriteriaQuery;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.mmedrek.revolut.model.Account;

import io.dropwizard.hibernate.AbstractDAO;

public class AccountDao extends AbstractDAO<Account>{

	public AccountDao(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public Optional<Account> getById(UUID id) {
		return Optional.ofNullable(get(id));
	}
	public Optional<Account> getByIdAndRefresh(UUID id) {
		Optional<Account> result = Optional.ofNullable(get(id));
		result.ifPresent(account -> refresh(account));
		return result;
	}

	public List<Account> getAll() {
		CriteriaQuery<Account> query = currentSession().getCriteriaBuilder().createQuery(Account.class);
		query.select(query.from(Account.class));
		return list(query);
	}
	
	public Account create(Account account) {
		return persist(account);
	}

	public void update(Account account) {
		currentSession().saveOrUpdate(account);
	}
	
	public void flush() {
		currentSession().flush();
	}
	
	public Transaction beginTransaction() {
		return currentSession().beginTransaction();
	}
	
	private void refresh(Account account) {
		currentSession().refresh(account);
	}
}
