package com.mmedrek.revolut.config;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

public class MoneyTransferConfig extends Configuration{

	@Valid
    @NotNull
    @JsonProperty("database")
    private DataSourceFactory database = new DataSourceFactory();

	@Valid
    @NotNull
    @JsonProperty("lock")
	private LockParams lockParams = new LockParams();
	
    public DataSourceFactory getDataSourceFactory() {
        return database;
    }
    
    public LockParams getLockParams() {
		return lockParams;
	}
	
    public static class LockParams{
    	private long waitTime;
    	
    	public long getWaitTime() {
    		return waitTime;
    	}
    }
	//TODO: extend this class when new global config is needed for MoneyTransfer
}
