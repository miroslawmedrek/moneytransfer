package com.mmedrek.revolut.utils;

import java.text.MessageFormat;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.dropwizard.jackson.Jackson;

public class ObjectUtils {

	private static final ObjectMapper OBJECT_MAPPER = Jackson.newObjectMapper();

	private ObjectUtils() {}
	
	public static String objectToString(Object object) {
		try {
			return OBJECT_MAPPER.writeValueAsString(object);
		} catch (JsonProcessingException e) {
			throw new SerializationException(
					MessageFormat.format("Cannot serialize object of class {0}", object.getClass().getName()), e);
		}
	}
	
	public static class SerializationException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public SerializationException(String message, Throwable ex) {
			super(message, ex);
		}
	}
}
