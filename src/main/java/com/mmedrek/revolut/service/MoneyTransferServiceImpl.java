package com.mmedrek.revolut.service;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.joda.money.CurrencyUnit;
import org.joda.money.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mmedrek.revolut.config.MoneyTransferConfig;
import com.mmedrek.revolut.db.AccountDao;
import com.mmedrek.revolut.exception.BlockingOperationInProgressException;
import com.mmedrek.revolut.exception.ValidationException;
import com.mmedrek.revolut.model.Account;
import com.mmedrek.revolut.model.TransferRequest;

public class MoneyTransferServiceImpl implements MoneyTransferService{

	private static final Logger LOGGER = LoggerFactory.getLogger(MoneyTransferServiceImpl.class);
	private AccountDao accountDao;
	private Map<UUID, LockWithCounter> locksByAccountId = new ConcurrentHashMap<>();
	private MoneyTransferConfig configuration;
	
	public MoneyTransferServiceImpl(AccountDao accountDao, MoneyTransferConfig configuration) {
		this.accountDao = accountDao;
		this.configuration = configuration;
	}

	@Override
	public void transfer(TransferRequest request) {
		LOGGER.debug("Processing request: {}", request);

		acquireLock(request);
		try {
			calculateTransfer(request);
		} finally {
			releaseLock(request);
		}
		LOGGER.debug("Request processed successfully");
	}
	
	/**Acquire locks only for requested accounts*/
	private void acquireLock(TransferRequest request) {
		locksByAccountId.compute(request.getSourceAccountId(), (k,v) -> getLock(k, v)).lock();
		locksByAccountId.compute(request.getTargetAccountId(), (k,v) -> getLock(k, v)).lock();
	}
	
	/**Release locks for requested accounts*/
	private void releaseLock(TransferRequest request) {
		locksByAccountId.computeIfPresent(request.getSourceAccountId(), (k,v) -> unlock(k, v));
		locksByAccountId.computeIfPresent(request.getTargetAccountId(), (k,v) -> unlock(k, v));
	}
	
	private LockWithCounter getLock(UUID key, LockWithCounter existingLock) {
		LockWithCounter lock = existingLock == null ? new LockWithCounter(configuration): existingLock;
		return lock;
	}
	
	private LockWithCounter unlock(UUID key, LockWithCounter existingLock) {
		LockWithCounter result = existingLock.waiting.get() == 1 ? null: existingLock; // 1 as only the current thread
		existingLock.unlock();
		return result;
	}

	private void calculateTransfer(TransferRequest request) {
		Account source = accountDao.getByIdAndRefresh(request.getSourceAccountId())
				.orElseThrow(() -> new AccountNotExistsException("Source account does not exist.", request));
		Account target = accountDao.getByIdAndRefresh(request.getTargetAccountId())
				.orElseThrow(() -> new AccountNotExistsException("Target account does not exist.", request));

		validateTheSameAccount(request, source, target);
		validateTransferBalance(request, source);
		LOGGER.debug("Request successfully validated");
		
		Money requestedMoney = Money.of(CurrencyUnit.of(request.getCurrency()), request.getAmount());
		Money sourceAccountMoney = Money.of(CurrencyUnit.of(source.getBalanceCurrencyCode()), source.getBalanceAmount());
		Money targetAccountMoney = Money.of(CurrencyUnit.of(target.getBalanceCurrencyCode()), target.getBalanceAmount());
		
		source.setBalance(sourceAccountMoney.minus(requestedMoney));
		target.setBalance(targetAccountMoney.plus(requestedMoney));
		
		accountDao.update(source);
		accountDao.update(target);
		
		accountDao.flush();
	}
	
	@FunctionalInterface
	private interface TransactionTask<T> {
		T execute();
	}
	
	private void validateTheSameAccount(TransferRequest request, Account source, Account target)  {
		if(source.getId().equals(target.getId())) {
			throw new TheSameAccountException("Source and target accounts are the same", request);
		}
	}
	
	private void validateTransferBalance(TransferRequest request, Account source) {
		CurrencyUnit requestedCurrencyUnit = CurrencyUnit.of(request.getCurrency()); 
		CurrencyUnit sourceCurrencyUnit = CurrencyUnit.of(source.getBalanceCurrencyCode());
		if(!requestedCurrencyUnit.equals(sourceCurrencyUnit)) {
			throw new BalanceException("Account currencies don't match", request);
		}
		
		Money requestedMoney = Money.of(requestedCurrencyUnit, request.getAmount());
		Money sourceAccountMoney = Money.of(sourceCurrencyUnit, source.getBalanceAmount());
		if(sourceAccountMoney.minus(requestedMoney).isNegative()) {
			throw new BalanceException("Unsuffiecient funds", request);
		}
	}
	
	private static class LockWithCounter {
		private AtomicInteger waiting = new AtomicInteger();
		private Lock lock = new ReentrantLock();
		private MoneyTransferConfig configuration;
		
		public LockWithCounter(MoneyTransferConfig configuration) {
			this.configuration = configuration;
		}
		
		public void lock() {
			waiting.incrementAndGet();
			tryLock();
		}
		
		public void unlock() {
			waiting.decrementAndGet();
			lock.unlock();
		}
		
		private void tryLock() {
			try {
				if (!lock.tryLock(configuration.getLockParams().getWaitTime(), TimeUnit.MILLISECONDS)) {
					throw new BlockingOperationInProgressException(
							"Lock cannot be exquired due to blocking operations");
				}
			} catch (InterruptedException e) {
				throw new IllegalStateException("Interruption not supported");
			}
		}
	}
	
	//--------------Exceptions-----------------
	static class AccountNotExistsException extends ValidationException {
		private static final long serialVersionUID = 1L;

		public AccountNotExistsException(String message, Object request) {
			super(message, request);
		}
	}
	
	static class TheSameAccountException extends ValidationException {
		private static final long serialVersionUID = 1L;

		public TheSameAccountException(String message, Object request) {
			super(message, request);
		}
	}
	
	static class BalanceException extends ValidationException {
		private static final long serialVersionUID = 1L;
		
		public BalanceException(String message, Object request) {
			super(message, request);
		}
	}
	
}
