package com.mmedrek.revolut.service;

import com.mmedrek.revolut.model.TransferRequest;

public interface MoneyTransferService {

	/**Transfers money between accounts*/
	void transfer(TransferRequest request);
}
