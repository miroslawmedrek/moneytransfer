# Money Transfer
This project was created to transfer money between internal accounts. 

# Starting
[Windows] To install and run the application run:
``` 
startApplication.bat
```

# Installation
[Windows] To just build jar file run:
``` 
buildJar.bat
```

# Testing
- Main directory contains `MoneyTransfer.postman_collection.json` file holding example payloads which can be used for integration testing in _Postman_. 
- Database was prepopulated with some mock data. You can find them under `\src\main\resources\db\migration\V1.2__MockData.sql`

# Key tools
 - DropWizard
 - Jackson
 - H2
 - Hibernate
 - Flyway
